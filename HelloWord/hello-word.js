//exo 1

console.log ("Hello word");


//exo 2

var http = require("http");
var fs = require ("fs");

http.createServer(function(request,response){
    response.writeHead(200,{'Content-Type': 'text/html'});
    fs.readFile('main.html', null, function(error,data){
        if (error){
            response.writeHead(404);
            response.write('Find not file');
        }else {
            response.write (data)
        }
    response.end();
    })
}).listen(3000);

console.log('Server running at http://localhost:3000');


//exo 3

var fs = require("fs")
var data = fs.readFileSync("welcome.txt");

console.log(data.toString());


//exo 4

var generator = require ('generate-password')
var password = generator.generate({
    length : 10,
    numbers : true
});

console.log(password);


//exo 5

var nodemailer = require ("nodemailer");
var transporter = nodemailer.createTransport({
    service : 'gmail',
    auth : {
        user : 'yourmail@gmail.com',
        password : 'mypassword'
    }
})
var mailOptions = {
    from : 'yourmail@gmail.com',
    to : 'myfriend@yahoo.com',
    subject : 'Je tenvoie un message avec Nodejs',
    text : 'Hey ! Ca va bien ?'
}
transporter.sendMail(mailOptions, function(error, info){
    if(error){
        console.log(error)
    } else {
        console.log('email'+ info.response)
    }
})