var http = require("http");
var fs = require('fs')

var app = http.createServer(function (request, response) {
    response.writeHead(200, {'Content-Type': 'text/html'});
    fs.readFile('index.html', 'index.css', function(error,data)  {
        if(error) {
            response.writeHead(404);
            response.write('File not found')
        } else{
            response.write(data);
        }
   
        response.end();
    })
 })
 app.listen(8081, () => {
    console.log('Server running at http://127.0.0.1:8081/');
 })
